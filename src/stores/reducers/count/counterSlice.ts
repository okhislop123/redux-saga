import { PayloadAction, createSlice } from "@reduxjs/toolkit";

const initialState = {
  count: 0,
  loading: false,
};

const counterSlice = createSlice({
  name: "count",
  initialState,
  reducers: {
    setCount: (state, action: PayloadAction<number>) => {
      state.count = action.payload;
    },
    increment: (state, action: PayloadAction<number>) => {
      console.log(action);
      state.loading = true;
    },
    incrementSuccess: (state, action: PayloadAction<number>) => {
      state.loading = false;
      state.count += action.payload;
    },
    decrement: (state, action: PayloadAction<number>) => {
      console.log(action);
      state.loading = true;
    },
    decrementSuccess: (state, action: PayloadAction<number>) => {
      state.loading = false;
      state.count -= action.payload;
    },
  },
});
export const {
  setCount,
  increment,
  incrementSuccess,
  decrement,
  decrementSuccess,
} = counterSlice.actions;
export default counterSlice.reducer;
