import { PayloadAction } from "@reduxjs/toolkit";
import { call, delay, put, takeEvery } from "redux-saga/effects";
import {
  decrement,
  decrementSuccess,
  increment,
  incrementSuccess,
} from "./counterSlice";
import { decrementCountAPI } from "../../../api/count";

export function* incrementCount(actions: PayloadAction<number>) {
  yield delay(2000);
  yield put(incrementSuccess(actions.payload));
}

export function* decrementCount(action: PayloadAction<number>) {
  const count: number = yield call(decrementCountAPI, action.payload);
  yield put(decrementSuccess(count));
}

export default function* counterSaga() {
  yield takeEvery(increment.toString(), incrementCount);
  yield takeEvery(decrement.toString(), decrementCount);
}
