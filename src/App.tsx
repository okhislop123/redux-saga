import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import "./App.css";
import { useAppDispatch, useAppSelector } from "./stores/hook";
import { Button, Spin } from "antd";
import { decrement, increment } from "./stores/reducers/count/counterSlice";

function App() {
  const dispatch = useAppDispatch();
  const countSlice = useAppSelector((store) => store.countSlice);

  return (
    <Spin spinning={countSlice.loading}>
      <div>
        <a href="https://vitejs.dev" target="_blank">
          <img src={viteLogo} className="logo" alt="Vite logo" />
        </a>
        <a href="https://react.dev" target="_blank">
          <img src={reactLogo} className="logo react" alt="React logo" />
        </a>
      </div>
      <h1>Count Saga: {countSlice.count}</h1>
      <br />
      <div>
        <Button onClick={() => dispatch(increment(1))} type="primary">
          Increment
        </Button>
        <span> </span>
        <Button type="primary" onClick={() => dispatch(decrement(1))}>
          Decrement
        </Button>
      </div>
    </Spin>
  );
}

export default App;
