export const decrementCountAPI = (number: number) => {
  return new Promise((res) => {
    setTimeout(() => {
      res(number);
    }, 2000);
  });
};
